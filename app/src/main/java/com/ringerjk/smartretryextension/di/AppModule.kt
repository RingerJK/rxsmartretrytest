package com.ringerjk.smartretryextension.di

import com.ringerjk.smartretryextension.support.AppLifecycle
import com.ringerjk.smartretryextension.support.AppLifecycleImpl
import org.koin.dsl.module.module

/**
 * @author Yury Kanetski.
 */

val appModule = module {
	single<AppLifecycleImpl> { AppLifecycleImpl() }
}