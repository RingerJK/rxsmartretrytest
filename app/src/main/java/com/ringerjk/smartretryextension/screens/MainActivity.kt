package com.ringerjk.smartretryextension.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ringerjk.smartretryextension.R
import com.ringerjk.smartretryextension.support.AppLifecycle
import com.ringerjk.smartretryextension.support.AppLifecycleImpl
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
	}
}
