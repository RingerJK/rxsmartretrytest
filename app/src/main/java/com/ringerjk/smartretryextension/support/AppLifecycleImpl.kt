package com.ringerjk.smartretryextension.support

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.ringerjk.smartretryextension.model.LifecycleAppEvent
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

/**
 * @author Yury Kanetski.
 */

class AppLifecycleImpl : LifecycleObserver, AppLifecycle {

	private val lifecycleAppSubject: Subject<LifecycleAppEvent> = BehaviorSubject.create()
	override val lifecycleAppObservable: Flowable<LifecycleAppEvent>
		get() = lifecycleAppSubject.toFlowable(BackpressureStrategy.LATEST)

	@OnLifecycleEvent(Lifecycle.Event.ON_START)
	fun onEnterForeground() {
		lifecycleAppSubject.onNext(LifecycleAppEvent.ON_FOREGROUND)
	}

	@OnLifecycleEvent(Lifecycle.Event.ON_STOP)
	fun onEnterBackground() {
		lifecycleAppSubject.onNext(LifecycleAppEvent.ON_BACKGROUND)
	}


}