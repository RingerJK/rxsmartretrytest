package com.ringerjk.smartretryextension.support

import com.ringerjk.smartretryextension.model.LifecycleAppEvent
import io.reactivex.Flowable

/**
 * @author Yury Kanetski.
 */
interface AppLifecycle {
	val lifecycleAppObservable: Flowable<LifecycleAppEvent>
}