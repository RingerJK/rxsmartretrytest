package com.ringerjk.smartretryextension.extensions

import com.ringerjk.smartretryextension.model.LifecycleAppEvent
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

/**
 * @author Yury Kanetski.
 */

fun <T> Flowable<T>.ioExceptionRetry(lifecycleAppObservable: Flowable<LifecycleAppEvent>, timeScheduler: Scheduler): Flowable<T> {
	val timerObservable = Flowable.interval(0, 1, TimeUnit.SECONDS, timeScheduler)
	val errTimeMills = AtomicLong()
	val errCount = AtomicInteger(0)
	return lifecycleAppObservable
			.switchMap {
				return@switchMap when (it) {
					LifecycleAppEvent.ON_FOREGROUND -> {
						this
					}
					LifecycleAppEvent.ON_BACKGROUND -> {
						errCount.set(0)
						Flowable.empty()
					}
				}
			}
			.doOnNext { if (errCount.get() != 0) errCount.set(0) }
			.retryWhen { flowErr ->
				return@retryWhen flowErr
						.takeWhile { err ->
							if (err !is IOException) {
								return@takeWhile false
							}
							if (errCount.get() == 0) {
								errTimeMills.set(System.currentTimeMillis())
							}
							return@takeWhile errCount.getAndIncrement() != 3 && (System.currentTimeMillis() - errTimeMills.get()) < 30_000
						}
						.flatMap { Flowable.just(Unit) }
			}
}