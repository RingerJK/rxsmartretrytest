package com.ringerjk.smartretryextension

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import com.ringerjk.smartretryextension.di.appModule
import com.ringerjk.smartretryextension.support.AppLifecycleImpl
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin

/**
 * @author Yury Kanetski.
 */

class App : Application() {

	private val appLifecycle: AppLifecycleImpl by inject()

	override fun onCreate() {
		super.onCreate()

		startKoin(this, listOf(appModule))

		ProcessLifecycleOwner.get().lifecycle.addObserver(appLifecycle)
	}
}