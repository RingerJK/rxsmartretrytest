package com.ringerjk.smartretryextension.model

/**
 * @author Yury Kanetski.
 */

enum class LifecycleAppEvent {
	ON_FOREGROUND,
	ON_BACKGROUND;
}