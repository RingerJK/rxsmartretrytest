package com.ringerjk.smartretryextension

import com.ringerjk.smartretryextension.extensions.ioExceptionRetry
import com.ringerjk.smartretryextension.model.LifecycleAppEvent
import com.ringerjk.smartretryextension.support.AppLifecycle
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subscribers.TestSubscriber
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.io.IOException
import java.util.concurrent.TimeUnit


/**
 * @author Yury Kanetski.
 */

class RxExtensionsTest {

	private val testScheduler = TestScheduler()
	private val disposables = CompositeDisposable()

	private val mockLifecycleSubject: BehaviorSubject<LifecycleAppEvent> = BehaviorSubject.create()
	private val mockLifecycleFlowable: Flowable<LifecycleAppEvent> = mockLifecycleSubject.toFlowable(BackpressureStrategy.LATEST)

	private var mockStreamEventsToIoException = false
	private val mockStreamSubject: PublishSubject<Int> = PublishSubject.create()
	private val mockStreamFlowable: Flowable<Int>
		get() = mockStreamSubject.toFlowable(BackpressureStrategy.LATEST)
				.map {
					if (mockStreamEventsToIoException) throw IOException("$it") else it
				}

	private lateinit var appLifecycle: AppLifecycle

	private fun appToBackground() = mockLifecycleSubject.onNext(LifecycleAppEvent.ON_BACKGROUND)

	private fun appToForeground() = mockLifecycleSubject.onNext(LifecycleAppEvent.ON_FOREGROUND)

	private fun setEvents(vararg event: Int) {
		event.forEach { mockStreamSubject.onNext(it) }
	}

	private fun getDefaultStream(): Flowable<Int> =
			mockStreamFlowable
					.ioExceptionRetry(appLifecycle.lifecycleAppObservable, testScheduler)
					.observeOn(testScheduler)

	private fun getSubscriber(): TestSubscriber<Int> = TestSubscriber<Int>().apply { addTo(disposables) }

	@Before
	fun `Before test`() {
		appToForeground()
		mockStreamEventsToIoException = false

		appLifecycle = Mockito.mock(AppLifecycle::class.java)
		Mockito.`when`(appLifecycle.lifecycleAppObservable).thenReturn(mockLifecycleFlowable)
	}

	@After
	fun `After test`() {
		disposables.clear()
	}

	@Test
	fun `Simple success`() {
		val subscriber = getSubscriber()
		getDefaultStream()
				.subscribe(subscriber)

		setEvents(1, 2, 30, 4, 5)
		testScheduler.triggerActions()
		subscriber.awaitCount(5)
		subscriber.assertValues(1, 2, 30, 4, 5)
	}

	@Suppress("UNUSED_CHANGED_VALUE")
	@Test
	fun `IO Exception 1-4 times`() {
		val subscriber = getSubscriber()
		getDefaultStream()
				.subscribe(subscriber)

		setEvents(1, 2)
		testScheduler.triggerActions()
		subscriber.assertNotTerminated()
		subscriber.assertValues(1, 2)

		mockStreamEventsToIoException = true

		setEvents(3, 4, 5)

		testScheduler.triggerActions()
		subscriber.assertNotTerminated()

		setEvents(6, 7)

		testScheduler.triggerActions()

		subscriber.assertTerminated()
		subscriber.assertNoErrors()
	}

	@Test
	fun `App lifecycle, scenario 1`() {
		val subscriber = getSubscriber()
		getDefaultStream()
				.subscribe(subscriber)

		mockStreamEventsToIoException = true
		setEvents(1, 2, 3)
		testScheduler.triggerActions()
		subscriber.assertNotTerminated()
		subscriber.assertNoValues()

		appToBackground()

		setEvents(4)
		testScheduler.triggerActions()
		subscriber.assertNoValues()

		appToForeground()

		setEvents(5, 6, 7)
		testScheduler.triggerActions()
		subscriber.assertNotTerminated()
		subscriber.assertNoValues()

		mockStreamEventsToIoException = false

		setEvents(8, 9, 10)
		testScheduler.triggerActions()
		subscriber.assertNotTerminated()
		subscriber.assertValues(8, 9, 10)
	}

	// TODO can't test it for option "do not retry if trying More than 30 seconds" without Thread.sleep(). As I know Thread.sleep() is a bad practice
	@Test
	fun `App lifecycle, scenario 2`() {
		val subscriber = getSubscriber()
		getDefaultStream()
				.subscribe(subscriber)

		mockStreamEventsToIoException = true

		setEvents(1)
		TimeUnit.SECONDS.sleep(20)
		setEvents(2)
		TimeUnit.SECONDS.sleep(10)
		setEvents(3)
		setEvents(4)

		subscriber.assertTerminated()
		subscriber.assertNoValues()

	}
}